/**
 *Simple TCP Server
 * @description this module is a simple web socket server
 * @author      Andrea Ferroni <andrea.ferroni.76@gmail.com>
 * @version     0.3.0

 *
 * @todo
 *  -
 *
 */

'use strict'

// dependencies
const WebSocket = require('ws'),            // websockets
  _ = require('lodash');                    // lodash

// default values
const defaultServerPort = 8080;
const defaultServerHost = '127.0.0.1';
const defaultServerPath = "/"

/**
 * simple web socket server moule
 * @module webSocketServer
*/
module.exports = class webSocketServer {

  /**
   * VERSION property accessor
   * @returns {string}
   * @static
   * @readonly
   */
  static get VERSION() { return "0.3.0" }

  /**
   * VERSION property cannot be mutated: it is an error!
   * @static
   * @throws {Error} inconditionally
   * @param {string} value any value
   * @returns {void}
   */
  static set VERSION(value) {
    throw new Error(`The readOnly property cannot be written. ${value} was passed.`);
  }

  constructor(port, path, name = '') {
    this.port = port
    this.path = path
    this.name = name
    this.isInitialized = false

    this.webSocketServer = undefined
    this.webSocket = undefined

    this.simulateContinuousSend = undefined

    this.debugFn = undefined

    this.consoleLogEverything = true
  }

  start() {
    // start server and wait a connection
    this.webSocketServer = new WebSocket.Server({
        port: this.port,
        path: this.path
    })

    this.webSocketServer.on('connection', ws => {

      // assign  webSocket property
      this.webSocket = ws

      // log web socket request
      let _remoteAddress = this.webSocket._sender._socket.remoteAddress
      console.log(`${this.name} web socket connection from ${_remoteAddress}`)

      this.webSocket.on('error', function(err) {
        console.error("WebSocket error:", err)
      })

      // Send Hey!
      try {
        this.webSocket.send('Hey!', function ack(error) {
          if (!_.isUndefined(error)) {
            console.log(`${this.name} socket error: ${error}`)
          }
        })
      }
      catch (e) {
        console.log(`${this.name} socket error: ${error}`)
      }

      this.webSocket.on('message', message => {
        if (message == "Oh!") {
          this.webSocket.send('Let\'s go!')
          let _remoteAddress = this.webSocket._sender._socket.remoteAddress
          this._outputMessage(`${this.name} web socket initialized from ${_remoteAddress}`, 'info')
          this.isInitialized = true
        }

        if ( this.isInitialized && _.isObject(this.simulateContinuousSend) ) {
            let closeThis = this
            setInterval(function() {
              closeThis.send(closeThis.simulateContinuousSend.fn())
            }, closeThis.simulateContinuousSend.to)
        }
      })
    })
  }

  send(msg) {
    if ( this.isInitialized === true && !_.isUndefined(this.webSocket) ) {
      this.webSocket.send(msg)
    }
  }

  _outputMessage(msg, type) {
    if ( _.isFunction(this.debugFn) ) {
      this.debugFn(type, msg)
    }
    else {
      console.log(msg)
    }

    if ( this.consoleLogEverything ) console.log(msg)
  }
}
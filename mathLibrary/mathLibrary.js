/**
 * Title:        Math Library
 * Type:         Library
 * @description: this library offers some math useful functions
 * @author:      Andrea Ferroni <andrea.ferroni.76@gmail.com>
 * Repo:         <git@gitlab.com:andrea.ferroni.76/shm-locator.git>
 * @version: 0.9.0
 *
 * @todo:
 *  - add some tests
 *
 */

// dependencies
const _ = require('lodash')


let mathFunctions = {

    /**
     * Compute distance between two angles in degrees
     *
     * @method twoAnglesDistanceDegree
     * @param {number} ang1 - first angle in degrees
     * @param {number} ang2 - second angle in degrees
     * @returns minimum distance in degree between ang1 and ang2 in a 0..359 range
     */
    twoAnglesDistanceDegrees: function (ang1, ang2) {
        let aMax = Math.max(ang1, ang2)
        let aMin = Math.min(ang1, ang2)
        let diff = aMax - aMin

        if (diff > 180) diff = aMin + 360 - aMax
        return diff
    },

    /**
     * Return minimum distance between two points, on xy-plane or in xyz-space
     *
     * @method twoPointsDistance
     * @param {object} [position={x, y}|{x, y, z}] object position on a plane o in space
     * @param {object} [position={x, y}|{x, y, z}] object position on a plane o in space
     * @returns minimum distance between two points
     */
    twoPointsDistance: function (p1, p2) {
        let xyDist = Math.pow((p1.x - p2.x), 2) + Math.pow((p1.y - p2.y), 2),
            z = 0
        if (typeof p1.z !== "undefined" || typeof p2.z !== "undefined") {
            z = Math.pow((p1.z - p2.z), 2)
        }

        return Math.sqrt(xyDist + z)
    },

    /**
     * Apply a 2D counterclockwise rotation to a point around an arbitrary point and get new coordinates
     *
     * @param {number} X - original point x-axis coordinate
     * @param {number} Y - original point y-axis coordinate
     * @param {number} angle - rotation angle to apply (degrees or radians)
     * @param {boolean} [isAngleDegree=true] - if false the parameter angle is in radians
     * @param {number} [originX=0] - rotation center x-axis coordinate; specify if different from 0 (origin x-axis)
     * @param {number} [originY=0] - rotation center y-axis coordinate; specify if different from 0 (origin y-axis)
     * @returns {x: number, y: number} - new poin coordinates
     */
    pointRotateCCW: function (X, Y, angle, isAngleDegree = true, originX = 0, originY = 0) {
        // convert degrees to radians if needed
        let _angle = isAngleDegree ? this.degrees2Radians(angle) : angle

        let _x = ((X - originX) * Math.cos(_angle)) - ((Y - originY) * Math.sin(_angle)) + originX
        let _y = ((X - originX) * Math.sin(_angle)) + ((Y - originY) * Math.cos(_angle)) + originY

        return {
            x: _x,
            y: _y
        }
    },

    degrees2Radians: function (angle) {
        return angle * (Math.PI / 180)
    },

    /**
     *
     * @param {number} a1 - angle1
     * @param {number} a2 - angle2
     * @param {number} t - threshold
     */
    angleIsNear: function(a1,a2,t) {
        return this.twoAnglesDistanceDegrees(a1, a2) <= t
    },

    radians2Degrees: function (angle) {
        return angle * (180 / Math.PI)
    },

    /**
     * @name add2AnglesDegrees
     *
     * @param {number} angle1
     * @param {number} angle2
     */
    add2AnglesDegrees: function (angle1, angle2, negativeAllowed = false) {
        let res = (angle1 + angle2) % 360
        if (negativeAllowed === true || res >= 0) {
            return res
        }
        else {
            return (360 + res)
        }
    },

    round: function (n, d, b) {
        let pow = Math.pow(b || 10, d)
        return Math.round(n * pow) / pow
    },

    getRandomIntInclusive(min, max) {
        min = Math.ceil(min)
        max = Math.floor(max)
        return Math.floor(Math.random() * (max - min + 1)) + min
    },

    isIntegerString(str) {
        return /^\+?(0|[1-9]\d*)$/.test(str)
    },

    isFloatString(str) {
        return parseFloat(str.match(/^-?\d*(\.\d+)?$/)) > 0
    },

    elementPositionInMatrix(matrix, el) {
        let res = false
        let matrixRoxNum = 0
        matrix.forEach(function (matrixRow, idx) {
            if (res) return
            let matrixColNum = matrixRow.indexOf(el)
            if (matrixColNum !== -1) {                    // element found
                res = [matrixRoxNum, matrixColNum]          // returning position
            }
            ++matrixRoxNum
        })

        return res                                          // element not found
    },

    // elements must be an array with "position" and "tag" properties; "postion" a 2D array,
    // "tag" can be anything (is a label). Eg: [{"position": [2,3], "tag": "ciao"}, {"position": [1,1], "tag": "prova"}]
    check2DSquarePositions(elements) {

        if (elements.length !== 4) return false

        let el0 = elements[0].position
        let el1 = elements[1].position
        let el2 = elements[2].position
        let el3 = elements[3].position

        // compute markers distances from the first one
        let distEl0El1 = [el0[0] - el1[0], el0[1] - el1[1]]
        let distEl0El2 = [el0[0] - el2[0], el0[1] - el2[1]]
        let distEl0El3 = [el0[0] - el3[0], el0[1] - el3[1]]

        // max distance per coordinate must be at max 1
        if (Math.abs(distEl0El1[0]) > 1 || Math.abs(distEl0El1[1]) > 1) return false
        if (Math.abs(distEl0El2[0]) > 1 || Math.abs(distEl0El2[1]) > 1) return false
        if (Math.abs(distEl0El3[0]) > 1 || Math.abs(distEl0El3[1]) > 1) return false

        // now place each marker in a square array...
        let res = [[undefined, undefined], [undefined, undefined]]
        res[0][0] = elements[0].tag
        res[Math.abs(distEl0El1[0])][Math.abs(distEl0El1[1])] = elements[1].tag
        res[Math.abs(distEl0El2[0])][Math.abs(distEl0El2[1])] = elements[2].tag
        res[Math.abs(distEl0El3[0])][Math.abs(distEl0El3[1])] = elements[3].tag

        // ...and check that the square is fully filled
        return ((_.compact(res[0])).length + (_.compact(res[1])).length == 4) ? true : false
    },

    check2DTrianglePositions(elements) {
        if (elements.length !== 3) return false

        let el0 = elements[0].position
        let el1 = elements[1].position
        let el2 = elements[2].position

        // compute markers distances from the first one
        let distEl0El1 = [el0[0] - el1[0], el0[1] - el1[1]]
        let distEl0El2 = [el0[0] - el2[0], el0[1] - el2[1]]

        // max distance per coordinate must be at max 1
        if (Math.abs(distEl0El1[0]) > 1 || Math.abs(distEl0El1[1]) > 1) return false
        if (Math.abs(distEl0El2[0]) > 1 || Math.abs(distEl0El2[1]) > 1) return false

        // now place each marker in a square array...
        let res = [[undefined, undefined], [undefined, undefined]]
        res[0][0] = elements[0].tag
        res[Math.abs(distEl0El1[0])][Math.abs(distEl0El1[1])] = elements[1].tag
        res[Math.abs(distEl0El2[0])][Math.abs(distEl0El2[1])] = elements[2].tag

        // ...and check that the square is fully filled
        return ((_.compact(res[0])).length + (_.compact(res[1])).length == 3) ? true : false
    },

    getMissingMarkerAsSquare(elements, md) {
        let minX = Math.min(..._.map(elements, (e) => e.position[0]))
        let maxX = Math.max(..._.map(elements, (e) => e.position[0]))
        let minY = Math.min(..._.map(elements, (e) => e.position[1]))
        let maxY = Math.max(..._.map(elements, (e) => e.position[1]))

        let fourMarkers = []
        fourMarkers.push(md.getByPos(minX, minY))
        fourMarkers.push(md.getByPos(minX, maxY))
        fourMarkers.push(md.getByPos(maxX, minY))
        fourMarkers.push(md.getByPos(maxX, maxY))

        let threeMarkers = _.map(elements, (e) => e.tag)

        return _.difference(fourMarkers, threeMarkers).join()
    },

    check2DRowPositions(elements) {
        if (elements.length !== 2) return false

        let el0 = elements[0].position
        let el1 = elements[1].position

        // compute markers distances from the first one
        let distEl0El1 = [el0[0] - el1[0], el0[1] - el1[1]]

        // max distance per coordinate must be at max 1
        if (Math.abs(distEl0El1[0]) > 1 || Math.abs(distEl0El1[1]) > 1) return false

        // distances sum must be 1
        let distancesSum = Math.abs(distEl0El1[0]) + Math.abs(distEl0El1[1])
        return (distancesSum === 1)
    }
}

module.exports = mathFunctions

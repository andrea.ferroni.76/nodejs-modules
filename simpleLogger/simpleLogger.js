// @ts-check

/**
 * Simple Logger
 * @fileOverview this module allows to log on file and/or console
 * @author      Andrea Ferroni <andrea.ferroni.76@gmail.com>
 * @version     1.0.0
 *
 * @requires NPM:lodash
 * @todo
 *  - add pathname/filename checks
 */

'use strict'

// dependencies
const fs = require('fs')
const _ = require('lodash')
const path = require('path')


/**
 * simple logger moule
 * @module simpleLogger
 */
module.exports = class simpleLogger {

  /**
   * VERSION property accessor
   * @returns {string}
   * @static
   * @readonly
   */
  static get VERSION() { return "1.0.0" }

  /**
   * VERSION property cannot be mutated: it is an error!
   * @static
   * @throws {Error} inconditionally
   * @param {string} value any value
   * @returns {void}
   */
  static set VERSION(value) {
    throw new Error(`The readOnly property cannot be written. ${value} was passed.`);
  }

  constructor() {

    this._develDebug = true

    // log caching settings (like Redis)
    this.fileCacheLimits = {
        requests: 300,                                          // requests before log
        seconds: 2                                              // seconds before log
    }
    this._fileCacheCounter = {
        requests: 0,                                            // log requests counter
        timeout: undefined,                                     // private file cache limit timeout property
    }
    // file buffer
    this._fileBuffer = new Array()
    // if set to true, every log request logs to console
    this.logToConsole = undefined
    // if set to true every log request log to file
    this.logToFile = undefined
    // log error throws error
    this.throwOnErr = false
    // is the log file name used if logToFile is true
    this.fileName = undefined
    // is the log file path used if logToFile is true
    this.filePath = undefined
    // logfile stream
    this._logFileStream = undefined
  }

  // set log flags and eventually open file stream if logToFile is true
  init(consoleLog, fileLog) {
    if (!_.isBoolean(consoleLog) && !_.isUndefined(consoleLog)) {
      console.error("Please pass boolean or undefined as first parameter")
      return
    }
    if (!_.isBoolean(fileLog) && !_.isUndefined(fileLog)) {
      console.error("Please pass boolean or undefined as second parameter")
      return
    }

    if (_.isUndefined(this.logToConsole)) {
      this.logToConsole = consoleLog
      if (_.isUndefined(this.logToConsole)) {
        this.logToConsole = true
      }
    }

    if (_.isUndefined(this.logToFile)) {
      this.logToFile = fileLog
      if (_.isUndefined(this.logToFile)) {
        this.logToFile = false
      }
    }

    if ( !_.isUndefined(this.fileName) ) {
      this.logToFile = true
    }

    if (this.logToFile === true) {
      this._initFile()
    }
  }

  _initFile() {
    if (_.isUndefined(this.filePath)) {
        console.error("Please set filePath property")
        return
    }
    if (_.isUndefined(this.fileName)) {
        console.error("Please set fileName property")
        return
    }

    let logFile = path.normalize(this.filePath + this.fileName)
    this._logFileStream = fs.createWriteStream(logFile, { flags: 'a' })
  }

  // log text according type severity on console / file according to configuration
  logThis(type, text, opt) {
    // check if both arguments are non-empty strings
    if (type === "" || text === "") {
      console.error("Please pass two non-empty paramaters")
      return
    }

    let logToConsole = this.logToConsole
    let logToFile = this.logToFile

    if (!_.isUndefined(opt)) {
      switch(opt) {
        case "console only":
          logToConsole = true
          logToFile = false
          break
        case "file only":
          logToConsole = false
          logToFile = true
          break
        case "not console":
          logToConsole = false
          break
        case "not file":
          logToFile = false
          break
        default:
          console.warn(`unknown parameter ${opt}`)
      }
    }

    let dateTime = new Date().toLocaleString()

    let logTextFinalPart = " " + dateTime + " | " + text + "\n"
    if (text === "\n") { logTextFinalPart = text }

    switch (type) {
      case "err":
        logToConsole && console.error(text)
        logToFile && this._fileWriteEnqueue("[ERR!] " + logTextFinalPart)
        if (this.throwOnErr) setTimeout(function () { throw text }, 750)
        break
      case "warn":
        logToConsole && console.warn(text)
        logToFile && this._fileWriteEnqueue("[WARN] " + logTextFinalPart)
        break
      case "info":
        logToConsole && console.info(text)
        logToFile && this._fileWriteEnqueue("[info] " + logTextFinalPart)
        break
      case "log":
        logToConsole && console.log(text)
        logToFile && this._fileWriteEnqueue("[log ] " + logTextFinalPart)
        break
      case "----":
        logToConsole && console.info("")
        logToFile && this._fileWriteEnqueue("\n\n\n")
        break
    }
  }

  _fileWriteEnqueue(text) {
    // if option "log to file" is disabled, do nothing and return
    if (!this.logToFile) return
    // enqueue text into buffer
    this._fileBuffer.push(text)

    // check and set timer and counter
    ++this._fileCacheCounter.requests                           // increment requests counter

    let closeThis = this
    if (_.isUndefined(this._fileCacheCounter.timeout)) {        // set timeout if not set
      this._fileCacheCounter.timeout = setTimeout(
        function () {
          this._develDebug && console.log(`${this.fileName} timer expired (${this.fileCacheLimits.seconds}s): flush`)
            closeThis.fileBufferFlush()                         // write data and reset counters
          },
          this.fileCacheLimits.seconds * 1000                   // timeout
      )
    }

    // check if counter is at its maximum
    if (this._fileCacheCounter.requests >= this.fileCacheLimits.requests) {
      this._develDebug && console.log(`${this.fileName} counter expired (${this._fileCacheCounter.requests}): flush`)
      closeThis.fileBufferFlush()
    }
  }

  fileBufferFlush() {
    if (this._fileBuffer.length != 0) {                         // is there something to write?
      let _allLines = this._fileBuffer.join("")                 // create text from buffer lines
      this._fileBuffer = []                                     // empty buffer lines
      this._logFileStream.write(_allLines)                      // write text
    }

    // reset counters
    this._fileCacheCounter.requests = 0                         // reset writes counter
    clearInterval(this._fileCacheCounter.timeout)               // reset interval counter
    this._fileCacheCounter.timeout = undefined                  // unset timeout structure
  }
}

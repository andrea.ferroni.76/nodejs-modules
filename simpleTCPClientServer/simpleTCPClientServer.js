// @ts-check

/**
 *Simple TCP Server
 * @description this module is a telnet-like server
 * @author      Andrea Ferroni <andrea.ferroni.76@gmail.com>
 * @version     0.3.0

 *
 * @todo
 *  - actually can't handle more than one connection... is really a simple TCP server!
 *  - either can handle multiple connection or can be newed as an object
 *
 */


// dependencies
const net = require('net')
const os = require('os')
//const { tcpServer } = require('../../shm-locator/src/conf')

const defaultServerPort = 8000
const defaultClientPort = 12345

/**
 * a simple TCP server module
 * @module tcpServer
*/
class tcpServer {

  /**
   * VERSION property accessor
   * @returns {string}
   * @static
   * @readonly
   */
  static get VERSION() { return "0.3.1" }

  /**
   * VERSION property cannot be mutated: is an error!
   * @static
   * @throws {Error} inconditionally
   * @param {string} value any value
   * @returns {void}
   */
  static set VERSION(value) {
    throw new Error(`The VERSION property cannot be written; ${value} was passed.`)
  }

  constructor() {
    this.EOT = String.fromCharCode(4)
    this.tcpServer = undefined
    this.isConsoleLogEnabled = true
    this.serverPort = defaultServerPort
    this.socket = undefined,
      this.customCallbacks = new Array()
    this.customInitCallbacks = new Array()
    this.openConnections = 0
  }

  get isListening() {
    return this.tcpServer.listening
  }

  _consoleLog(txt) {
    this.isConsoleLogEnabled ? console.log(txt) : undefined
  }

  /**
   * NOT YET TESTED !
   * @param {*} text
   */
  send(text) {
    this.socket.write(`${text}\r\n`)
    this.socket.pipe(this.socket)
  }

  init() {
    let closeThis = this
    this.tcpServer = net.createServer(function (socket) {

      this.socket = socket

      closeThis.tcpServer.getConnections(function (error, count) {
        closeThis.openConnections = count
      })

      socket.on('data', function (data) {
        let _dummyDate = new Date()
        let timeString = _dummyDate.getHours().toString().padStart(2, '0') + ":" + _dummyDate.getMinutes().toString().padStart(2, '0') + "." + _dummyDate.getSeconds().toString().padStart(2, '0')
        let text = `data received at ${timeString}`
        closeThis._consoleLog(text)

        let dataText = data.toString().trimRight()

        if (dataText === closeThis.EOT) {
          socket.destroy()
        }

        for (let cb of closeThis.customCallbacks) {
          cb.func(dataText, socket)
        }
      }).on('error', (err) => {
        console.log(err)
        // handle errors outside
        throw err;
      });;

      for (let icb of closeThis.customInitCallbacks) {
        icb.func(socket)
      }
    })

    return this;
  }


  addDataCallback(fn) {
    if (fn == null) {
      throw new TypeError
    }

    return this.customCallbacks.push({
      func: fn
    }) - 1
  }

  /**
   * Add TCP socket initialization callback
   *
   * @static
   * @memberOf tcpServer
   * @since 0.2.0
   * @param {function} fn function to call during new socket initialization
   * @returns {number} Returns the index of the callback
   * @example
   *
   * tcpServer.addNewSocketCallback(function(){
   *   console.log("new connection incoming");
   * });
   * // => 0
   *
   * tcpServer.addNewSocketCallback(function(socket){
   *   console.log("from " + );
   * });
   * // => 1
   */
  addNewSocketCallback(fn) {
    if (fn == null) {
      throw new TypeError;
    }

    return this.customInitCallbacks.push({
      func: fn
    }) - 1
  }

  listen() {
    this.tcpServer.listen(this.serverPort);
    this._consoleLog(`· TCP server started listening on port ${this.serverPort}`);    // server is listening
  }

  close(cb) {
    this.tcpServer.close(cb);
  }
}


/**
 * a simple TCP client module
 * @module tcpClient
*/
class tcpClient {

  /**
   * VERSION property accessor
   * @returns {string}
   * @static
   * @readonly
   */
  static get VERSION() { return "0.0.1" }

  /**
   * VERSION property cannot be mutated: is an error!
   * @static
   * @throws {Error} inconditionally
   * @param {string} value any value
   * @returns {void}
   */
  static set VERSION(value) {
    throw new Error(`The readOnly property cannot be written. ${value} was passed.`)
  }

  constructor() {
    this.EOT = String.fromCharCode(4)
    this.tcpClient = undefined
    this.isConsoleLogEnabled = true
    this.isConnected = false
    this.clientPort = defaultClientPort
    this.serverAddress = '127.0.0.1'
    this.socket = undefined
    this.customCallbacks = new Array()
    this.customInitCallbacks = new Array()
    this.openConnections = 0
    this.keepAlive = 1000
    this.timeout = 10000
    this.sendEOL = true
    this.sendEOT = false
  }

  _consoleLog(txt) {
    this.isConsoleLogEnabled ? console.log(txt) : undefined
  }

  connect() {

    let closeThis = this

    if (typeof this.tcpClient === "undefined") this.tcpClient = new net.Socket()

    if (this.tcpClient.connecting) return

    this._consoleLog(`Connecting to server ${this.serverAddress}:${this.clientPort}...`)
    this.tcpClient.connect(this.clientPort, this.serverAddress, function () {
      closeThis._consoleLog('Connected.')
      closeThis.isConnected = true

      // set timeout
      if ( closeThis.timeout ) {
        closeThis.tcpClient.setTimeout(1000)

        closeThis.tcpClient.on('timeout', () => {
          closeThis._consoleLog(`Connection timeout (${closeThis.timeout})`)
          closeThis.isConnected = false
          closeThis.tcpClient.destroy()
        })
      }

      // set keepalive
      if (closeThis.keepAlive) {
        closeThis.tcpClient.setKeepAlive(true, this.keepAlive)
      } else {
        closeThis.tcpClient.setKeepAlive(false)
      }

      closeThis.tcpClient.on('data', function (data) {
        closeThis.customCallbacks.forEach(function (v) {
          v(data)
        })
      })

      closeThis.tcpClient.on('close', function () {
        closeThis.isConnected = false
        closeThis._consoleLog('Connection closed')
      })
    })


  }

  send(t) {

    if ( typeof t !== "string" ) throw new TypeError

    // send data and eventually also EOL / EOT
    this.isConnected && this.tcpClient.write(t)                         // send
    this.isConnected && this.sendEOL && this.tcpClient.write(os.EOL)    // EOL
    this.isConnected && this.sendEOT && this.tcpClient.write(this.EOT)  // EOT

    // try again to connect to server is connection is still not up
    if (!this.isConnected) {
      this._consoleLog('Can\'t send data: connection not established')
      this._consoleLog('Trying to connect again')
      this.connect()
    }
  }

  addDataCallback(fn) {
    if (fn == null) {
      throw new TypeError
    }

    return this.customCallbacks.push(fn) - 1
  }

  close() {
    this.tcpClient.destroy()
  }
}

module.exports = {
  server: tcpServer,
  client: tcpClient
}
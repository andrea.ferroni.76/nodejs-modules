/**
 * Title:        Simple Web Server
 * Type:         Library
 * @description: this module is a web server for static files; it can also
 * handle custom requests too
 * @author:      Andrea Ferroni <andrea.ferroni.76@gmail.com>
 * Repo:         <git@gitlab.com:andrea.ferroni.76/shm-locator.git>
 * @version:     0.9.0
 * 
 * @todo:
 *  - add a parameter to specify if empty replies are allowed, such as {} in _sendJSON() or "" in _sendText()
 *  - check if method `start` is called before `init`
 *  - add jsDoc
 *
 */


// dependencies
const http = require('http'),
      url = require('url'),
      path = require('path'),
      fs = require('fs'),
      _ = require('lodash');

// default values
const defaultServerPort = 8000;
const defaultServerAddress = '127.0.0.1';

// implementation
var webServer = {

    webServer: undefined,

    isConsoleLogEnabled: true,

    serverPort: defaultServerPort,

    serverAddress: defaultServerAddress,

    rootDir: '/var/www',

    customCalls: new Array(),

    CONSTSTRINGS: {
        "mimeType": {
            '.ico': 'image/x-icon',
            '.html': 'text/html',
            '.js': 'text/javascript',
            '.mjs': 'text/javascript',
            '.json': 'application/json',
            '.css': 'text/css',
            '.png': 'image/png',
            '.jpg': 'image/jpeg',
            '.wav': 'audio/wav',
            '.mp3': 'audio/mpeg',
            '.svg': 'image/svg+xml',
            '.pdf': 'application/pdf',
            '.doc': 'application/msword',
            '.eot': 'appliaction/vnd.ms-fontobject',
            '.ttf': 'aplication/font-sfnt'
        },
        "headerHTML": { 'Content-Type': 'text/html' },
        "headerJSON": { 'Content-Type': 'application/json' }
    },

    _consoleLog: function (txt) {
        this.isConsoleLogEnabled ? console.log(txt) : undefined;
    },

    _checkStatus: function (status) {
        if (!_.isInteger(status)) { _consoleLog("webServer._sendJSON() status parameter must be an integer"); return false }
        if (_.isUndefined(http.STATUS_CODES[status])) { _consoleLog("webServer._sendJSON() status parameter must be a valid HTTP status code"); return false }
        return true;
    },

    // Send text as reply to client
    _sendText: function (text, destination, status = 200) {
        // verify text parameter
        if (!_.isString(text)) {
            _consoleLog("webServer._sendJSON() text parameter must be a string, even if empty");
            return false
        }
        // verify status parameter (error logging is inside the function itself)
        if (!this._checkStatus(status)) return false

        let res = this._send(text, destination, this.CONSTSTRINGS.headerHTML, status)

        return res
    },

    // Send JSON object as reply to client
    /**
     * @method _sendJSON
     * @private
     * @description wraps _send method for specific JSON reply
     * 
     * @param {*} JSONdata data to send
     * @param {*} destination client
     * @param {*} status HTTP status code
     * 
     * @returns {boolean} whether data are succesfully sent or not
     */
    _sendJSON: function (JSONdata, destination, status = 200) {
        // verify JSONdata parameter
        if (!_.isObject(JSONdata)) {
            _consoleLog("webServer._sendJSON() JSONdata parameter must be an valid JSON object")
            return false
        }
        // verify status parameter (error logging is inside the function itself)
        if (!this._checkStatus(status)) return false

        let json = JSON.stringify(JSONdata)
        let res = this._send(json, destination, this.CONSTSTRINGS.headerJSON, status)

        return res
    },

    _send: function (payload, destination, headers, status = 200) {
        try {
            destination.writeHead(status, headers);
            destination.end(payload);
        }
        catch (e) {
            _consoleLog("webServer._send() something went wrong");
            return false;
        }
        return true;
    },

    /**
     * @method init
     * @public
     * @description initialize web server. Call this method before calling start
     */
    init: function () {
        var closeThis = this;
        var server = this.webServer;
        this.webServer = http.createServer((req, res) => {

            // parse URL
            const parsedUrl = url.parse(req.url, true);
            // extract URL path
            // Avoid https://en.wikipedia.org/wiki/Directory_traversal_attack
            // e.g curl --path-as-is http://localhost:9000/../fileInDanger.txt
            // by limiting the path to current directory only
            const sanitizePath = path.normalize(parsedUrl.pathname).replace(/^(\.\.[\/\\])+/, '');
            // get requested HTTP method
            let method = req.method.toUpperCase();

            // log the request
            closeThis._consoleLog(`[${method}] call to ${req.url}`);                 // log richiesta

            // PARSE AND (TRY TO) SERVE CUSTOM CALLS
            let customCallServed = false;
            for (let i = 0; i < closeThis.customCalls.length; i++) {
                // test regExp
                if (this.customCalls[i].urlRegExp.test(req.url)) {
                    // run function
                    var reply = this.customCalls[i].func(req.url);
                    // send reply according requested type
                    switch (reply.type.toUpperCase()) {
                        case "JSON":
                            this._sendJSON(reply.data, res);
                            customCallServed = true;
                            break;
                        case "TEXT":
                            this._sendText(reply.data, res);
                            customCallServed = true;
                            break;
                    }
                }
            }
            if (customCallServed) {
                closeThis._consoleLog(`custom call ${req.url} served`);
                return;
            }

            // SERVE FILE
            let pathname = path.join(this.rootDir, sanitizePath);
            fs.exists(pathname, function (exist) {
                if (!exist) {
                    // if the file is not found, return 404
                    res.statusCode = 404;
                    res.end(`File ${pathname} not found!`);
                    return;
                }
                // if is a directory, then look for index.html
                if (fs.statSync(pathname).isDirectory()) {
                    pathname += 'index.html';
                }
                // read file from file system
                fs.readFile(pathname, function (err, data) {
                    if (err) {
                        res.statusCode = 500;
                        res.end(`Error getting the file: ${err}.`);
                        closeThis._consoleLog(`Error getting the file: ${err}.`);
                    } else {
                        // based on the URL path, extract the file extention. e.g. .js, .doc, ...
                        const ext = path.parse(pathname).ext.toLowerCase();
                        // if the file is found, set Content-type and send data
                        res.setHeader('Content-type', closeThis.CONSTSTRINGS.mimeType[ext] || 'text/plain');
                        res.end(data);
                        closeThis._consoleLog(`Served file ${pathname}`);
                    }
                });
            });
        });
    },

    addCustomCall: function (regEx, fn) {
        this.customCalls.push({
            urlRegExp: regEx,
            func: fn
        });
    },

    start: function () {
        const callback = () => {
            const address = this.webServer.address().address;
            const port = this.webServer.address().port;
            this._consoleLog(`· HTTP server started at http://${address}:${port}\n`);
        };

        this.webServer.listen(
            this.serverPort,
            this.serverAddress,
            callback
        );
    }
};

module.exports = webServer;

'use strict'

//
// nodejs libraries
const fs = require('fs');
const path = require('path')


let waitCharsSequece = function(loopForever, seqId, interval) {

  if (!process.stdin.isTTY) return

  let seq
  let defaultSeq = [
    "⠀⢀⣀⣄⣤⣴⣶⣷⣿⢿⠿⠟⠛⠙⠉⠁",
    "▁▂▃▅▆▇▉▊▋▍▎▏▏▎▍▋▊▉▇▆▅▃▂▁",
    "☱☲☴☴☴☲☱☱",
    "⬊⬇⬋⬅⬉⬆⬈⬈⬈⬆⬉⬅⬋⬇⬊⬊⬊",
    "⫷⪡⪕⪙⪛",
    "⫧⫩⫨⫨⫨⫩⫧⫧",
    "𝌆𝌇𝌉𝌏𝌒𝌓𝌊𝌇𝌆𝌆"
  ]
  let idx = 0
  let defaultInterval = 120
  if (typeof(seqId) === "undefined" ) seq = defaultSeq[1]
  if (typeof(interval === "undefined") ) interval = defaultInterval

  function _startTimeout() {
    setTimeout(function(){
      process.stdout.clearLine()  // clear current text
      process.stdout.cursorTo(0)  // move cursor to beginning of line
      process.stdout.write(seq.charAt(idx))
      idx++
      if (!loopForever) {
        if (idx < seq.length) _startTimeout()
      } else {
        idx = idx % seq.length
        _startTimeout()
      }
    }, interval)
  }

  console.log("\n")
  _startTimeout()
}
/**
 * Depends on fs and path libraries
 * @param {*} appDir
 */
let outputAsciiArtBanner = function(basePath) {
  try {
    let asciiArtFileName = path.join(basePath, "asciiart.txt")
    let asciiArtFileContent = fs.readFileSync(asciiArtFileName, "utf8")
    console.log(asciiArtFileContent)
  }
  catch(e) {
    // none
  }
}

let getArgs = function() {
  const args = {};
  process.argv
    .slice(2, process.argv.length)
    .forEach( arg => {
    // long arg
    if (arg.slice(0,2) === '--') {
      const longArg = arg.split('=');
      const longArgFlag = longArg[0].slice(2,longArg[0].length);
      const longArgValue = longArg.length > 1 ? longArg[1] : true;
      args[longArgFlag] = longArgValue;
    }
    // flags
    else if (arg[0] === '-') {
      const flags = arg.slice(1,arg.length).split('');
      flags.forEach(flag => {
      args[flag] = true;
      });
    }
  });
  return args;
}

class loggerUtils {

  separator = '_'
  extension = 'log'

  constructor(dirName, basePath, moduleName) {
    this.dirName = dirName
    this.basePath = basePath
    this.moduleName = moduleName
    this.initBuffer = ''
  }

  fileName(name) {
    return `${this.moduleName}${this.separator}${name}.${this.extension}`
  }

  get path() {
    return `${this.dirName}/${this.basePath}log/`
  }

  initLogger(conf, logger) {
    // set filename and path
    logger.fileName = this.fileName(conf.logName)
    logger.filePath = this.path
    // init
    logger.init(conf.logToConsole, conf.logToFile)

    // debug
    let loggerOptions = []
    if (conf.logToConsole ) loggerOptions.push("console")
    if (conf.logToFile ) loggerOptions.push("file")
    if (loggerOptions.length === 0) loggerOptions.push("NOTHING !?!?")

    this.initBuffer = `Logger ${conf.logName} initialized [${logger.fileName}]- logs to: ${loggerOptions.join(" and ")}`
  }
}

let formatItalianDate = function(date) {
  let options = { weekday: 'long', year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' };
  return date.toLocaleDateString("it-IT", options);
}

module.exports = {outputAsciiArtBanner, getArgs, loggerUtils, formatItalianDate, waitCharsSequece}